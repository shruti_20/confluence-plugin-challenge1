package com.adaptavist.recruitment.confluence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adaptavist.recruitment.confluence.utils.CommonUtils;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.EntityException;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;

public class MyPluginComponentImpl implements MyPluginComponent, Macro
{

  private static final Logger log = LoggerFactory.getLogger(MyPluginComponentImpl.class);
  private final BootstrapManager bootstrapManager;
  private final UserManager userManager;
  private final PageManager pageManager;

  public MyPluginComponentImpl(BootstrapManager bootstrapManager, UserManager userManager, PageManager pageManager,
      UserAccessor userAccessor)
  {
    this.bootstrapManager = bootstrapManager;
    this.userManager = userManager;
    this.pageManager = pageManager;
  }

  public String getRelativeBasePath()
  {
    return this.bootstrapManager.getWebAppContextPath();
  }

  public boolean hasBody()
  {
    return false;
  }

  public RenderMode getBodyRenderMode()
  {
    return RenderMode.NO_RENDER;
  }

  private String getFullName(String username) throws MacroException
  {
    try
    {
      User user = this.userManager.getUser(username);
      if (user == null)
        return username;
      return user.getFullName();
    }
    catch (EntityException e)
    {
      log.error("Error while trying to retrieve the page author", e);
      throw new MacroException(e.getMessage());
    }
  }

  /**
   * Get user names for the different type of users
   */
  public String getUserString(Map<String, String> parameters, RenderContext renderContext, ContentEntityObject entity)
      throws MacroException
  {
    String displayType = (String) parameters.get("0");
    String prefix = (String) parameters.get("prefix");
    if (prefix == null)
    {
      prefix = "";
    }
    Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
    contextMap.put("basepath", getRelativeBasePath());
    contextMap.put("prefix", prefix);
    List<String> usernames = new ArrayList<String>();
    List<String> fullnames = new ArrayList<String>();
    if ((displayType.indexOf("created") > -1)
        || ((displayType.indexOf("modified") > -1) && (displayType.indexOf("users") < 0)))
    {
      String username;
      if (displayType.indexOf("created") > -1)
        username = entity.getCreatorName();
      else
      {
        username = entity.getLastModifierName();
      }

      if (username == null)
      {
        fullnames.add("Anonymous");
        usernames.add("");
      }
      else
      {
        fullnames.add(getFullName(username));
        usernames.add(username);
      }
    }
    else
    {
      Iterator iterator;
      if ((displayType.equalsIgnoreCase("participants")) || (displayType.equalsIgnoreCase("commenters"))
          || (displayType.indexOf("users") > -1))
      {
        SortedSet users = new TreeSet();
        if (!displayType.equalsIgnoreCase("commenters"))
        {
          users.add(entity.getCreatorName());
          users.add(entity.getLastModifierName() == null ? entity.getCreatorName() : entity.getLastModifierName());

          List revisions = this.pageManager.getVersionHistorySummaries(entity);
          for (iterator = revisions.iterator(); iterator.hasNext();)
          {
            Object revision = iterator.next();
            VersionHistorySummary summary = (VersionHistorySummary) revision;
            String modifier = summary.getLastModifierName();
            users.add(modifier == null ? "" : modifier);
          }
        }

        if (((entity instanceof AbstractPage))
            && ((displayType.equalsIgnoreCase("commenters")) || (displayType.equalsIgnoreCase("participants"))))
        {
          List comments = ((AbstractPage) entity).getComments();
          for (iterator = comments.iterator(); iterator.hasNext();)
          {
            Object comment = iterator.next();
            ContentEntityObject o = (ContentEntityObject) comment;
            String commenter = o.getCreatorName();
            users.add(commenter == null ? "" : commenter);
          }

        }

        for (iterator = users.iterator(); iterator.hasNext();)
        {
          Object user = iterator.next();
          String username = (String) user;
          usernames.add(username);
          if (username.equals(""))
            fullnames.add("Anonymous");
          else
            fullnames.add(getFullName(username));
        }
      }
      else
      {
        throw new MacroException("Unrecognised display type [" + displayType + "]");
      }
    }

    return displayContributors(parameters, contextMap, usernames, fullnames);

  }

  private String displayContributors(Map<String, String> parameters, Map<String, Object> contextMap,
      List<String> usernames, List<String> fullnames) throws MacroException
  {
    boolean wrap = "false".equalsIgnoreCase((String) parameters.get("wrap"));
    contextMap.put("usernames", usernames);
    contextMap.put("fullnames", fullnames);
    contextMap.put("wrap", wrap ? "true" : "false");
    String vmtype = "list".equals(parameters.get("type")) ? "-list.vm" : "-flat.vm";
    try
    {
      return VelocityUtils.getRenderedTemplate("vm/users" + vmtype, contextMap);
    }
    catch (Exception e)
    {
      log.error("Error while trying to display the page author", e);
      throw new MacroException(e.getMessage());
    }
  }

  /*
   * Fetch the parameter for the type of user to be displayed and display user names
   */
  public String renderMacro(Map<String, String> parameters, String body, RenderContext renderContext)
      throws MacroException
  {
    String displayType = (String) parameters.get("0");
    if (displayType == null)
    {
      throw new MacroException("A display type is Required");
    }
    String pageTarget = (String) parameters.get("page");

    ContentEntityObject entity = CommonUtils.getPageEntity(renderContext, pageTarget, this.pageManager);
    if (entity == null)
    {
      throw new MacroException("unable to locate page");
    }

    if ((displayType.indexOf("user") > -1) || (displayType.equalsIgnoreCase("participants"))
        || (displayType.equalsIgnoreCase("commenters")))
      return getUserString(parameters, renderContext, entity);

    throw new MacroException("Unrecognised display type3 [" + displayType + "]");
  }

  @Override
  public String execute(Map<String, String> params, String body, ConversionContext conversionContext)
      throws MacroExecutionException
  {
    try
    {
      return renderMacro(params, body, conversionContext.getPageContext());
    }
    catch (MacroException e)
    {
      throw new MacroExecutionException(e.getMessage(), e);
    }
  }

  public Macro.BodyType getBodyType()
  {
    return Macro.BodyType.NONE;
  }

  public Macro.OutputType getOutputType()
  {
    return Macro.OutputType.INLINE;
  }
}
