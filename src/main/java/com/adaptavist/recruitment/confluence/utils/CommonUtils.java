package com.adaptavist.recruitment.confluence.utils;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.RenderContext;

public class CommonUtils
{
  public static ContentEntityObject getPageEntity(RenderContext renderContext, String pageTarget,
      PageManager pageManager)
  {
    ContentEntityObject entity = null;
    if ((renderContext instanceof PageContext))
    {
      if ((pageTarget == null) || (pageTarget.equalsIgnoreCase("@self")))
      {
        entity = ((PageContext) renderContext).getEntity();
      }
      else if (pageTarget.equalsIgnoreCase("@parent"))
      {
        entity = ((PageContext) renderContext).getEntity();
        if ((entity instanceof Page))
          entity = ((Page) entity).getParent();
        else
          entity = null;
      }
      else
      {
        String spaceKey = null;
        String pageTargetFallback = pageTarget;
        int colonIndex = pageTarget.indexOf(":");

        if (colonIndex > 1)
        {
          spaceKey = pageTarget.substring(0, colonIndex);
          pageTarget = pageTarget.substring(colonIndex + 1);
        }
        else
        {
          spaceKey = ((PageContext) renderContext).getSpaceKey();
        }
        entity = pageManager.getPage(spaceKey, pageTarget);

        if (entity == null)
        {
          entity = pageManager.getPage(((PageContext) renderContext).getSpaceKey(), pageTargetFallback);
        }
      }
    }

    return entity;
  }
}