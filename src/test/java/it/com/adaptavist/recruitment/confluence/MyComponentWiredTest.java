package it.com.adaptavist.recruitment.confluence;

import org.junit.runner.RunWith;

import com.adaptavist.recruitment.confluence.MyPluginComponentImpl;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;

@RunWith(AtlassianPluginsTestRunner.class)
public class MyComponentWiredTest {
    private final ApplicationProperties applicationProperties;
    private final MyPluginComponentImpl myPluginComponent;

    public MyComponentWiredTest(ApplicationProperties applicationProperties, MyPluginComponentImpl myPluginComponent) {
        this.applicationProperties = applicationProperties;
        this.myPluginComponent = myPluginComponent;
    }

    /*@Test
    public void testMyName() {
        assertEquals("names do not match!", "myComponent:" + applicationProperties.getDisplayName(), myPluginComponent.getName());
    }*/
}
